# Testing genome-wide association

This repository provides codes and brief explanations on how to perform genome-wide association analysis using the Genome Association and Prediction Integrated Tool (GAPIT). Examples are provides by the GAPIT R-package.

# References

* Lipka, A. E., Tian, F., Wang, Q., Peiffer, J., Li, M., Bradbury, P. J., ... & Zhang, Z. (2012). GAPIT: genome association and prediction integrated tool. Bioinformatics, 28(18), 2397-2399.
* Tang, Y., Liu, X., Wang, J., Li, M., Wang, Q., Tian, F., ... & Zhang, Z. (2016). GAPIT version 2: an enhanced integrated tool for genomic association and prediction. The plant genome, 9(2), plantgenome2015-11.
* Wang, J., & Zhang, Z. (2021). GAPIT Version 3: boosting power and accuracy for genomic association and prediction. Genomics, proteomics & bioinformatics, 19(4), 629-640.

# Key topics covered

* ...

# Acknowledgements 

This material is part of the **Genomics of Environmental Adaptation** winter school and draws on the expertise of all instructors and lecturers.

# Support

Please, send an email to benjamin.dauphin@wsl.ch or christian.rellstab@wsl.ch with a reproducible example if a problem occurs when running the scripts.

# Contributing

Any contribution or suggestion to improve or supplement this project is welcome. Please open an issue to discuss what you would like to change or contact us directly via email.

# License

GNU AGPLv3.

# Project status

This project is primarily used for teaching purposes and is therefore updated on a non-regular basis.
